#pragma once

// Standard/CUDA Includes
#include "cuda_runtime.h"
#include "device_launch_parameters.h"
#include <curand.h>
#include <curand_kernel.h>
#include <iostream>

// User defined Libraries
#include "ModelParams.cu.h"
#include "DLConfig.cu.h"

